# Alketap-release



![Reciters list](https://gitlab.com/tayee/alketap-release/-/raw/4e57fda5b0c55b244ebc7c4050d63cbecbc869a7/screenshots/home.png?inline=false)
![Reciter page](https://gitlab.com/tayee/alketap-release/-/raw/master/screenshots/reciter-page.png?inline=false)
## Getting started

Alketap! A free app that collects a list of most of the world's reciters with all Qur'an Rewayah

## Current features

- [x] Page for all reciters with search
- [x] Page for every reciter with surah search 

## Upcoming features

- [ ] Rewayat Page
- [ ] Filter reciters based on the selected Rewayah
- [ ] Ayah with Audio 
- [ ] Tafseer Alquran
- [ ] Release chrome extension  
